import { atom } from 'recoil';

import { Post } from '../@types/common';

const commentModalAtom = atom<{
  isOpen: boolean, selectedPost:Post | null, selectedPostId:string|null
}>({
  key: 'commentModal',
  default: {
    isOpen: false,
    selectedPost: null,
    selectedPostId: null,
  },
});

export default commentModalAtom;
