import { useState, useEffect } from 'react';
import Image from 'next/image';
import {
  collection,
  DocumentData,
  onSnapshot,
  orderBy,
  query,
  QueryDocumentSnapshot,
} from '@firebase/firestore';

import ComposeSection from './Compose';
import Header from './Haeder';
import Post from './Post';

import { db } from '../services/firebase';

import type { Post as PostType } from '../@types/common';

import Loading from '../assets/images/Loading.svg';

const Feed = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [posts, setPosts] = useState<QueryDocumentSnapshot<DocumentData>[]>([]);

  useEffect(() => {
    setIsLoading(true);
    const unsubscribe = onSnapshot(query(collection(db, 'posts'), orderBy('timestamp', 'desc')), (snapshot) => {
      setPosts(snapshot.docs);
      setIsLoading(false);
    });
    return unsubscribe;
  }, [db]);

  return (
    <section className="max-w-2xl justify-between grow-up px-3 py-3 border-l border-r border-gray-200 ml-[73px] xl:ml-[370px] flex-grow ">
      <Header isHomePage />
      <ComposeSection />
      {
      !isLoading
        ? (
          <div className="pb-70">
            {
            posts.map((post) => (
              <Post key={post.id} content={post.data() as PostType} postId={post.id} />
            ))
          }
          </div>
        )
        : (
          <div className="flex justify-center items-center pt-24 m-auto">
            <Image src={Loading} alt="loading" width={50} height={50} />
          </div>
        )
     }
    </section>
  );
};

export default Feed;
