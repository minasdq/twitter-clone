import { SearchIcon } from '@heroicons/react/outline';

import { Follower } from '../@types/common';

interface WidgetsProps {
  suggestedFollowers: Follower[];
}

const Widgets = ({ suggestedFollowers }: WidgetsProps) => (
  <div className="hidden md:inline ml-8 w-[300px] xl:w-[450px] py-1 space-y-5">
    <div className="sticky top-0 py-1.5 z-50 w-11/12 xl:w-9/12">
      <div className="flex items-center bg-[#eff3f4] p-3 rounded-full relative">
        <SearchIcon className="text-gray-500 h-5 z-50" />
        <input
          type="text"
          className="bg-transparent placeholder-gray-500 outline-none text-[#0f1419] absolute inset-0 pl-11 border border-transparent w-full focus:border-[#1d9bf0] rounded-full focus:bg-white focus:shadow-lg"
          placeholder="Search Twitter"
        />
      </div>
    </div>

    <div className="text-[#0f1419] bg-[#f7f9f9] space-y-3 pt-2 mr-3 rounded-xl xl:w-9/12">
      <h4 className="font-bold text-xl px-4">Who to follow</h4>
      {suggestedFollowers.map((suggestedFollower) => (
        <div
          className="hover:bg-white hover:bg-opacity-[0.03] px-4 py-2 cursor-pointer transition duration-200 ease-out flex items-center"
          key={suggestedFollower.tag}
        >
          <img
            alt="follower"
            src={suggestedFollower.userImage}
            width={50}
            height={50}
            className="rounded-full object-contain "
          />
          <div className="ml-4 leading-5 group">
            <h4 className="font-bold group-hover:underline">
              {suggestedFollower.username}
            </h4>
            <h5 className="text-gray-500 text-[15px]">{suggestedFollower.tag}</h5>
          </div>
          <button type="button" className="ml-auto bg-[#272c30] text-white rounded-full font-bold text-sm py-1.5 px-3.5">
            Follow
          </button>
        </div>
      ))}
      <button
        type="button"
        className="hover:bg-white hover:bg-opacity-[0.03] px-4 py-3 cursor-pointer transition duration-200 ease-out flex items-center justify-between w-full text-[#1d9bf0]"
      >
        Show more
      </button>
    </div>
  </div>
);

export default Widgets;
