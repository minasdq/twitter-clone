import { useState, useEffect } from 'react';
import {
  collection, DocumentData, onSnapshot, orderBy, query, QueryDocumentSnapshot,
} from '@firebase/firestore';
import Image from 'next/image';
import { useSession } from 'next-auth/react';
import Moment from 'react-moment';

import {
  ChatIcon, ChartBarIcon, HeartIcon, ShareIcon, SwitchVerticalIcon, TrashIcon,
} from '@heroicons/react/outline';

import { db } from '../services/firebase';

import Loading from '../assets/images/Loading.svg';

interface CommentProps {
    postId: string;
}

const Comment = ({ postId }: CommentProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [comments, setComments] = useState<QueryDocumentSnapshot<DocumentData>[]>([]);

  const { data: session } = useSession();

  useEffect(
    () => {
      setIsLoading(true);
      const unsubscribe = onSnapshot(
        query(
          collection(db, 'posts', postId, 'comments'),
          orderBy('timestamp', 'desc'),
        ),
        (snapshot) => setComments(snapshot.docs),
      );
      setIsLoading(false);

      return unsubscribe;
    },
    [db, postId],
  );

  return (
    <div className="pb-70">
      {!isLoading
        ? (
          <div className="w-full pb-2.5">
            <div className="flex relative flex-col">
              {(comments).map((comment) => (
                <div className="border-b pb-4 pt-3 px-4" key={comment.id}>
                  <div className="flex">
                    <img src={comment.data().userImage} className="w-10 h-10 rounded-full" alt="profile" />
                    <div className="ml-2.5 mt-1">
                      <div className="inline-block group">
                        <h4
                          className="sm:text-base marker:text-[#0f1419] font-bold text-[15px] group-hover:underline inline-block"
                        >{comment.data().username}
                        </h4>
                        <span className="text-sm sm:text-[15px] text-[#536471] ml-1.5">@{comment.data().tag}</span>
                        <span className="text-sm sm:text-[15px] text-[#536471]">
                          {' '}.{' '}
                          <Moment fromNow>{comment.data().timestamp?.toDate()}</Moment>
                        </span>
                      </div>
                      <div className="flex ml-2.5 mt-1">
                        {comment.data().text && (
                        <p className="text-[#0f1419] text-[18px] break-words">{comment.data().text}</p>)}
                      </div>
                    </div>
                  </div>
                  <div className="flex justify-between items-center text-[#536471] ml-2.5 mt-7 w-10/12">
                    <div className="icon group-hover:bg-opacity-10">
                      <ChatIcon className="h-5 group-hover:text-[#1d9bf0]" />
                    </div>
                    {session!.user.uid === comment.data().id ? (
                      <div aria-hidden className="icon group hover:bg-red-600 hover:bg-opacity-10">
                        <TrashIcon className="h-5 group-hover:text-red-600" />
                      </div>
                      ) : (
                        <div className="icon group hover:bg-green-500 hover:bg-opacity-10">
                          <SwitchVerticalIcon className="h-5 group-hover:text-green-500" />
                        </div>
                        )}
                    <div className="icon group-hover:bg-pink-600 group-hover:bg-opacity-10">
                      <HeartIcon className="h-5 group-hover:text-pink-600" />
                    </div>
                    <div className="icon group hover:bg-opacity-10">
                      <ShareIcon className="h-5 group-hover:text-[#1d9bf0]" />
                    </div>
                    <div className="icon group hover:bg-opacity-10">
                      <ChartBarIcon className="h-5 group-hover:text-[#1d9bf0]" />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )
        : (
          <div className="flex justify-center items-center pt-24 m-auto">
            <Image src={Loading} alt="loading" width={50} height={50} />
          </div>
        )}
    </div>
  );
};

export default Comment;
