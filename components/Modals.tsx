import { useRecoilValue } from 'recoil';

import CommentModal from './modals/CommentModal';

import { commentModal } from '../atoms';

const Modals = () => {
  const isCommentModalOpen = useRecoilValue(commentModal);
  return isCommentModalOpen && <CommentModal />;
};

export default Modals;
