interface SidebarProps {
    Icon: React.FC<React.PropsWithChildren<React.ComponentProps<'svg'>>>;
    text: string;
    active?: boolean;
}

const SidebarLink = ({ Icon, text, active }: SidebarProps) => (
  <div className={`text-[#0f1419] flex
   items-center justify-center xl:justify-start text-lg 
   space-x-3 hoverAnimation ${active && 'font-bold'}`}
  >
    <Icon className="h-7" />
    <span className="hidden xl:inline">{text}</span>
  </div>
);

export default SidebarLink;
