import { ChangeEvent, useState, useRef } from 'react';
import data from '@emoji-mart/data';
import Picker from '@emoji-mart/react';
import { useSession } from 'next-auth/react';
import {
  addDoc,
  collection,
  doc,
  serverTimestamp,
  updateDoc,
} from '@firebase/firestore';
import { getDownloadURL, ref, uploadString } from '@firebase/storage';

import { EmojiHappyIcon, PhotographIcon, XIcon } from '@heroicons/react/outline';

import { db, storage } from '../services/firebase';

const Input = () => {
  const [post, setPost] = useState('');
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState<string | null>('');

  const fileInputRef = useRef<HTMLInputElement | null>(null);

  const { data: session } = useSession();

  const addPost = async () => {
    setIsLoading(true);

    const documentRef = await addDoc(collection(db, 'posts'), {
      id: session!.user.uid,
      username: session!.user.name,
      userImage: session!.user.image,
      tag: session!.user.tag,
      text: post,
      timestamp: serverTimestamp(),
    });

    if (selectedFile) {
      const imageRef = ref(storage, `posts/${documentRef.id}/image`);
      await uploadString(imageRef, selectedFile, 'data_url');
      const downloadURL = await getDownloadURL(imageRef);
      await updateDoc(doc(db, 'posts', documentRef.id), {
        image: downloadURL,
      });
    }

    setIsLoading(false);
    setPost('');
    setSelectedFile(null);
    setShowEmojiPicker(false);
  };

  const addEmoji = (e:any) => {
    const sym = e.unified.split('-');
    const codesArray = sym.map((el:any) => `0x${el}`);
    const emoji = String.fromCodePoint(...codesArray);
    setPost((prevPost) => prevPost + emoji);
  };

  const addPhoto = (event: ChangeEvent<HTMLInputElement>) => {
    const reader = new FileReader();
    const file = (event.target! as HTMLInputElement).files?.[0];
    if (file) {
      reader.readAsDataURL(file);
    }

    reader.onload = (readerEvent) => {
      setSelectedFile(readerEvent.target!.result as string);
    };
  };

  return (
    <section className="border-b border-gray-200 flex p-3 space-y-3 overflow-y-scroll scrollbar-hide">
      <img src={session?.user.image!} className="h-10 w-10 rounded-full cursor-pointer " alt="avatar" />
      <div className={`divide-y divide-gray-200 w-full ${selectedFile && 'pb-7'} ${post && 'space-y-2.5'} ${isLoading && 'opacity-60'}`}>
        <textarea
          value={post}
          onChange={(event) => setPost(event.target.value)}
          placeholder="What's happening?"
          rows={2}
          className="min-h-[60px] w-full placeholder:text-lg tracking-wide ml-3 outline-none text-[#0f1419]"
        />
        {selectedFile && (
        <div className="flex justify-center p-4">
          <div className="relative">
            <div
              aria-hidden
              className="absolute w-8 h-8 bg-[#15181c] hover:bg-[#272c26] bg-opacity-75 rounded-full flex items-center justify-center top-2 left-2 cursor-pointer"
              onClick={() => setSelectedFile(null)}
            >
              <XIcon className="text-white h-5" />
            </div>
            <img
              src={selectedFile}
              alt="selected file"
              className="rounded-2xl max-h-[400px] object-contain"
            />
          </div>

        </div>
        )}
        {
        !isLoading && (
        <div className="flex items-center justify-between py-3 ">
          <div className="flex items-center space-x-4">
            <div className="icon w-9 h-9">
              <PhotographIcon className="text-[#1d9bf0] h-[22px]" onClick={() => fileInputRef.current?.click()} />
              <input type="file" ref={fileInputRef} onChange={addPhoto} hidden />
            </div>
            <div
              aria-hidden="true"
              className="icon w-9 h-9"
              onClick={() => setShowEmojiPicker((prevState) => !prevState)}
            >
              <EmojiHappyIcon className="text-[#1d9bf0] h-[22px]" />
            </div>
            {showEmojiPicker && data && (
            <div className="absolute ml-[-40px] mt-[465px]">
              <Picker
                data={data}
                onEmojiSelect={addEmoji}
                previewPosition="none"
                theme="light"
              />
            </div>
            )}
          </div>
          <button
            type="submit"
            className="bg-[#1d9bf0] text-white rounded-full px-4 py-1.5 font-bold shadow-md hover:bg-[#1a8cd8] disabled:hover:bg-[#1d9bf0] disabled:opacity-50 disabled:cursor-default"
            disabled={!post.trim() && !selectedFile}
            onClick={addPost}
          >
            Tweet
          </button>
        </div>
        )
       }
      </div>
    </section>

  );
};

export default Input;
