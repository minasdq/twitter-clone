import Image from 'next/image';
import { useSession, signOut } from 'next-auth/react';

import { HomeIcon } from '@heroicons/react/solid';
import {
  BellIcon, BookmarkIcon, ChatIcon, DotsCircleHorizontalIcon,
  DotsHorizontalIcon, HashtagIcon, PlusIcon, UserIcon, UsersIcon,
} from '@heroicons/react/outline';

import SidebarLink from './SidebarLink';

import Twitter from '../assets/images/Twitter.png';

const Sidebar = () => {
  const { data: session } = useSession();

  return (
    <aside className="flex flex-col align-center fixed p-2 xl:items-start xl:w-[340px] h-full">
      <div className="flex items-center justify-center w-14 h-14 icon p-0 xl:ml-24">
        <Image src={Twitter} height={34} width={34} />
      </div>
      <nav className="space-y-2.5 mb-2.5 xl:ml-24">
        <SidebarLink Icon={HomeIcon} text="Home" active />
        <SidebarLink Icon={HashtagIcon} text="Explore" />
        <SidebarLink Icon={UsersIcon} text="Communities" />
        <SidebarLink Icon={BellIcon} text="Notifications" />
        <SidebarLink Icon={ChatIcon} text="Messages" />
        <SidebarLink Icon={BookmarkIcon} text="Bookmarks" />
        <SidebarLink Icon={UserIcon} text="Profile" />
        <SidebarLink Icon={DotsCircleHorizontalIcon} text="More" />
      </nav>
      <button
        type="button"
        className="hidden xl:inline ml-auto
     bg-[#1d9bf0] text-white rounded-full w-56 h-[52px] text-lg font-bold shadow-md hover:bg-[#1a8cd8] "
      >
        Tweet
      </button>
      <button
        type="button"
        className="xl:hidden bg-[#1d9bf0] text-white rounded-full flex mx-auto
      p-2 text-lg font-bold shadow-md hover:bg-[#1a8cd8]"
      >
        <PlusIcon className="h-7" />
      </button>
      <div
        aria-hidden
        className="text-[#d9d9d9] flex items-center xl:w-56 justify-center mt-auto hoverAnimation xl:ml-auto overflow-hidden"
        onClick={() => signOut()}
      >
        <img
          src={session?.user.image!}
          alt=""
          className="h-10 w-10 rounded-full xl:mr-2.5"
        />
        <div className="hidden xl:inline leading-5">
          <h4 className="font-bold truncate text-[#0f1419]">{session?.user.name}</h4>
          <p className="text-[#536471] truncate">@{session?.user.tag}</p>
        </div>
        <DotsHorizontalIcon className="h-5 hidden xl:inline ml-10" />
      </div>
    </aside>
  );
};

export default Sidebar;
