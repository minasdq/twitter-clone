import { MouseEvent, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';
import { useSetRecoilState } from 'recoil';
import Moment from 'react-moment';
import {
  collection, deleteDoc, doc, DocumentData, onSnapshot, setDoc, QueryDocumentSnapshot,
} from '@firebase/firestore';

import {
  ChatIcon, ChartBarIcon, DotsHorizontalIcon,
  HeartIcon as OutlineHeartIcon, ShareIcon, SwitchVerticalIcon, TrashIcon,
} from '@heroicons/react/outline';
import { HeartIcon as SolidHeartIcon } from '@heroicons/react/solid';

import { db } from '../services/firebase';
import { commentModal } from '../atoms';

import type { Post as PostType } from '../@types/common';

interface PostProps {
    content: PostType;
    inDetail?: boolean
    postId: string;
}

const Post = ({ content, inDetail, postId }: PostProps) => {
  const [likes, setLikes] = useState<QueryDocumentSnapshot<DocumentData>[]>([]);
  const [comments, setComments] = useState<QueryDocumentSnapshot<DocumentData>[]>([]);
  const [isLiked, setIsLiked] = useState(false);

  const { data: session } = useSession();
  const router = useRouter();
  const setCommentModal = useSetRecoilState(commentModal);

  useEffect(() => onSnapshot(
    collection(db, 'posts', postId, 'likes'),
    (snapshot) => setLikes(snapshot.docs),
  ), [db, postId]);

  useEffect(() => onSnapshot(
    collection(db, 'posts', postId, 'comments'),
    (snapshot) => setComments(snapshot.docs),
  ), [db, postId]);

  useEffect(() => {
    setIsLiked(
      likes.findIndex((like) => like.id === session?.user?.uid) !== -1,
    );
  }, [likes]);

  const deletePost = async (event:MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    await deleteDoc(doc(db, 'posts', postId));
  };

  const likePost = async (event:MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    if (isLiked) {
      await deleteDoc(doc(db, 'posts', postId, 'likes', session!.user.uid));
    } else {
      setDoc(doc(db, 'posts', postId, 'likes', session!.user.uid), {
        username: session!.user.name,
      });
    }
  };

  return (
    (
      <article aria-hidden className="flex p-3 border-b border-gray-200 cursor-pointer" onClick={() => router.push(postId)}>
        <img src={content.userImage} alt="profile" className="rounded-full w-10 h-10" />
        <div className="w-full">
          <div className="flex w-full justify-between ml-2.5">
            <div className="inline-block group">
              <h4
                className={`sm:text-base marker:text-[#0f1419] font-bold text-[15px] group-hover:underline ${!inDetail && 'inline-block'}`}
              >{content.username}
              </h4>
              <span className={`text-sm sm:text-[15px] text-[#536471] ${!inDetail && 'ml-1.5'}`}>@{content.tag}</span>
              {!inDetail && (
              <span className="text-sm sm:text-[15px] text-[#536471]">
                {' '}.{' '}
                <Moment fromNow>{content.timestamp?.toDate()}</Moment>
              </span>
              )}
            </div>
            <div className="flex h-7 w-7 icon justify-center items-center">
              <DotsHorizontalIcon className="h-5 text-[#536471] hover:text-[#1d9bf0]" />
            </div>
          </div>
          <div className="ml-2.5 mt-1">
            {content.text && (
            <p className="text-[#0f1419] text-[18px]">{content?.text}</p>)}
            {content.image && (
            <img src={content.image} alt="post" className="object-fit rounded-2xl max-h-[400px] my-3 mx-auto" />)}
          </div>
          <div className="flex justify-between items-center text-[#536471] ml-2.5 mt-7 w-10/12">
            <div
              aria-hidden
              className="flex items-center space-x-1"
              onClick={(event) => {
                event.stopPropagation();
                setCommentModal({
                  isOpen: true,
                  selectedPost: content,
                  selectedPostId: postId,
                });
              }}
            >
              <div className="icon group-hover:bg-opacity-10">
                <ChatIcon className="h-5 group-hover:text-[#1d9bf0]" />
              </div>
              {
                comments.length > 0 && <span className="group-hover:text-[#1d9bf0] text-sm">{comments.length}</span>
              }
            </div>
            {session!.user.uid === content.id ? (
              <div aria-hidden className="icon group hover:bg-red-600 hover:bg-opacity-10" onClick={deletePost}>
                <TrashIcon className="h-5 group-hover:text-red-600" />
              </div>
              ) : (
                <div className="icon group hover:bg-green-500 hover:bg-opacity-10">
                  <SwitchVerticalIcon className="h-5 group-hover:text-green-500" />
                </div>
            )}
            <div aria-hidden onClick={likePost} className="flex items-center space-x-1 group">
              <div className="icon group-hover:bg-pink-600 group-hover:bg-opacity-10">
                {isLiked ? <SolidHeartIcon className="h-5 text-pink-600" />
                  : <OutlineHeartIcon className="h-5 group-hover:text-pink-600" />}
              </div>
              {likes.length > 0 && (
              <span className={`group-hover:text-pink-600 text-sm ${isLiked && 'text-pink-600'}`}>{likes.length}
              </span>
              )}
            </div>
            <div className="icon group hover:bg-opacity-10">
              <ShareIcon className="h-5 group-hover:text-[#1d9bf0]" />
            </div>
            <div className="icon group hover:bg-opacity-10">
              <ChartBarIcon className="h-5 group-hover:text-[#1d9bf0]" />
            </div>
          </div>
        </div>
      </article>
    )
  );
};

export default Post;
