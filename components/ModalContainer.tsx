import { ReactNode } from 'react';
import { SetterOrUpdater } from 'recoil';

import { Dialog, Transition } from '@headlessui/react';
import { XIcon } from '@heroicons/react/outline';

type StateType= any & {isOpen: boolean};

interface ModalContainerProps {
    modalState: StateType;
    setModalState: SetterOrUpdater<StateType>;
    children: ReactNode;
}

const ModalContainer = ({
  modalState, setModalState, children,
}: ModalContainerProps) => (
  <Transition.Root show={modalState.isOpen}>
    <Dialog
      as="div"
      className="fixed z-50 inset-0 pt-8"
      onClose={() => setModalState((prevState: StateType) => ({
        ...prevState,
        isOpen: false,
      }))}
    >
      <div className="flex items-start justify-center min-h-[800px] sm:min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <Transition.Child
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Dialog.Overlay className="fixed inset-0 bg-[#424242] bg-opacity-40 transition-opacity" />
        </Transition.Child>

        <Transition.Child
          enter="ease-out duration-300"
          enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          enterTo="opacity-100 translate-y-0 sm:scale-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100 translate-y-0 sm:scale-100"
          leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
        >
          <div className="bg-white inline-block align-bottom rounded-2xl text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full">
            <div className="flex items-center px-1.5 py-2 border-b border-gray-200 ml-auto">
              <div
                aria-hidden
                className="hoverAnimation w-9 h-9 flex items-center justify-center xl:px-0 ml-auto"
                onClick={() => setModalState((prevState: StateType) => ({
                  ...prevState,
                  isOpen: false,
                }))}
              >
                <XIcon className="h-[22px] text-[#0f1419]" />
              </div>
            </div>
            <div className="flex px-4 pt-5 pb-2.5 sm:px-6">
              {children}
            </div>
          </div>
        </Transition.Child>
      </div>
    </Dialog>
  </Transition.Root>
);
export default ModalContainer;
