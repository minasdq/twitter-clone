import { useRouter } from 'next/router';

import { ArrowLeftIcon, SparklesIcon } from '@heroicons/react/outline';

interface HeaderProps {
    isHomePage?: boolean;
}

const Header = ({ isHomePage } : HeaderProps) => {
  const router = useRouter();
  return (
    <div className="w-full">
      <div className="text-[#0f1419] flex items-center sticky top-0 z-50 bg-white">
        {!isHomePage && (
          <div
            aria-hidden
            className="hoverAnimation w-9 h-9 flex items-center justify-center xl:px-0 mr-5"
            onClick={() => router.push('/')}
          >
            <ArrowLeftIcon className="h-5" />
          </div>
        )}
        <h2 className="text-lg font-bold">{isHomePage ? 'Home' : 'Tweet'}</h2>
        {
            isHomePage && (
            <div className="hoverAnimation w-9 h-9 flex items-center justify-center xl:px-0 ml-auto">
              <SparklesIcon className="h-5" />
            </div>
            )
        }
      </div>
    </div>
  );
};

export default Header;
