import Image from 'next/image';
import { BuiltInProviderType } from 'next-auth/providers';
import {
  ClientSafeProvider, LiteralUnion, signIn,
} from 'next-auth/react';

import Twitter from '../assets/images/Twitter.png';

interface LoginProps{
  providers: Record<LiteralUnion<BuiltInProviderType, string>, ClientSafeProvider>
}

const Login = ({ providers }: LoginProps) => (
  <div className="flex flex-col justify-center items-center space-y-20 w-full">
    <div className="flex items-center justify-center w-40 h-40 icon">
      <Image src={Twitter} height={150} width={150} />
    </div>
    {
        Object.values(providers).map((provider) => (
          <button
            type="button"
            className="shadow-md rounded-md px-3.5 py-2 m-1 overflow-hidden relative group cursor-pointer border-2 font-medium border-[#1d9bf0] text-[#1d9bf0]"
            key={provider.name}
            onClick={() => signIn(provider.id, {
              callbackUrl: '/',
            })}
          >
            <span className="absolute w-64 h-0 transition-all duration-300 origin-center rotate-45 -translate-x-20  bg-[#1d9bf0] top-1/2 group-hover:h-64 group-hover:-translate-y-32 ease" />
            <span className="relative text-[#1d9bf0] transition duration-300 group-hover:text-white ease">
              Sign in with {provider.name}
            </span>
          </button>

        ))
    }
  </div>
);

export default Login;
