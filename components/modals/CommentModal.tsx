import { useState } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import { useRecoilState } from 'recoil';
import { useSession } from 'next-auth/react';
import Moment from 'react-moment';
import { addDoc, collection, serverTimestamp } from '@firebase/firestore';

import ModalContainer from '../ModalContainer';

import { commentModal as commentModalAtom } from '../../atoms';
import { db } from '../../services/firebase';

import Loading from '../../assets/images/Loading.svg';

const CommentModal = () => {
  const [commentModal, setCommentModal] = useRecoilState(commentModalAtom);
  const router = useRouter();
  const { data: session } = useSession();

  const [input, setInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const sendComment = async () => {
    setIsLoading(true);
    await addDoc(collection(db, 'posts', commentModal.selectedPostId!, 'comments'), {
      id: session?.user.uid,
      text: input,
      username: session?.user.name,
      tag: session?.user.tag,
      userImage: session?.user.image,
      timestamp: serverTimestamp(),
    });
    setIsLoading(false);
    setInput('');
    setCommentModal((prevState) => ({
      ...prevState,
      isOpen: false,
    }));
    router.push(commentModal!.selectedPostId!);
  };

  return (
    <ModalContainer modalState={commentModal} setModalState={setCommentModal}>
      <div className="w-full">
        <div className="flex relative flex-col">
          <span className="h-full w-0.5 z-[-1] absolute bg-gray-200 left-5 top-11" />
          <div className="flex mb-16">
            <img src={commentModal.selectedPost?.userImage} className="w-10 h-10 rounded-full" alt="profile" />
            <div className="ml-2.5 mt-1">
              <div className="inline-block group">
                <h4
                  className="sm:text-base marker:text-[#0f1419] font-bold text-[15px] group-hover:underline inline-block"
                >{commentModal.selectedPost?.username}
                </h4>
                <span className="text-sm sm:text-[15px] text-[#536471] ml-1.5">@{commentModal.selectedPost?.tag}</span>

                <span className="text-sm sm:text-[15px] text-[#536471]">
                  {' '}.{' '}
                  <Moment fromNow>{commentModal.selectedPost?.timestamp?.toDate()}</Moment>
                </span>
              </div>
              <div className="flex ml-2.5 mt-1">
                {commentModal.selectedPost?.text && (
                <p className="text-[#0f1419] text-[18px] break-words">{commentModal.selectedPost?.text}</p>)}
                {commentModal.selectedPost?.image && (
                <p className="text-[18px] break-words underline text-[#14004a]">{commentModal.selectedPost?.image}</p>)}
              </div>
            </div>
          </div>

          <div className="flex">
            <img src={session?.user.image!} className="w-10 h-10 rounded-full" alt="profile" />
            <textarea
              value={input}
              onChange={(event) => setInput(event.target.value)}
              placeholder="Tweet your reply"
              rows={1}
              className="min-h-[60px] w-full placeholder:text-lg tracking-wide ml-3 outline-none text-[#0f1419]"
            />
          </div>
        </div>
        {
          !isLoading ? (
            <button
              onClick={sendComment}
              type="submit"
              className="ml-auto flex mt-16 bg-[#1d9bf0] text-white rounded-full px-4 py-1.5 font-bold shadow-md hover:bg-[#1a8cd8] disabled:hover:bg-[#1d9bf0] disabled:opacity-50 disabled:cursor-default"
              disabled={!input.trim()}
            >
              Reply
            </button>
          ) : (
            <div className="flex justify-end h-10">
              <Image src={Loading} alt="loading" />
            </div>
          )
        }
      </div>
    </ModalContainer>
  );
};

export default CommentModal;
