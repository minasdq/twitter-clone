import type { NextPage } from 'next';
import Head from 'next/head';
import { getProviders, useSession } from 'next-auth/react';
import axios from 'axios';

import Feed from '../components/Feed';
import Login from '../components/Login';
import Sidebar from '../components/Sidebar';
import Modals from '../components/Modals';
import Widgets from '../components/Widgets';

import { Follower, NextAuthProviders } from '../@types/common';

interface HomeProps{
  providers: NextAuthProviders;
  suggestedFollowers: Follower[];
}

const Home: NextPage<HomeProps> = ({ suggestedFollowers, providers }) => {
  const { data: session } = useSession();

  return (
    <>
      <Head>
        <title>Twitter</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="min-h-screen flex max-w-[1500px] mx-auto">
        {
        session ? (
          <>
            <Sidebar />
            <Feed />
            {suggestedFollowers && <Widgets suggestedFollowers={suggestedFollowers} />}
          </>
        ) : <Login providers={providers} />
       }
        <Modals />
      </main>
    </>
  );
};

export default Home;

export const getServerSideProps = async () => {
  const providers = await getProviders();
  const suggestedFollowers = await axios.get('https://jsonkeeper.com/b/TDY0');

  return {
    props: {
      providers,
      suggestedFollowers: suggestedFollowers.data,
    },
  };
};
