import { useState, useEffect } from 'react';
import type { NextPage } from 'next';
import Image from 'next/image';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { getProviders, useSession } from 'next-auth/react';
import axios from 'axios';
import { doc, DocumentData, onSnapshot } from '@firebase/firestore';

import Comments from '../components/Comments';
import Header from '../components/Haeder';
import Login from '../components/Login';
import Sidebar from '../components/Sidebar';
import Modals from '../components/Modals';
import Post from '../components/Post';
import Widgets from '../components/Widgets';

import { db } from '../services/firebase';

import { Follower, NextAuthProviders, Post as PostType } from '../@types/common';

import Loading from '../assets/images/Loading.svg';

interface PostPageProps {
  providers: NextAuthProviders;
  suggestedFollowers: Follower[];
}

const PostPage: NextPage< PostPageProps > = ({ providers, suggestedFollowers }) => {
  const [isLoading, setIsLoading] = useState(false);
  const { data: session } = useSession();
  const router = useRouter();

  const [post, setPost] = useState<DocumentData | null >(null);

  const { postId } = router.query;

  useEffect(() => {
    setIsLoading(true);
    const unsubscribe = onSnapshot(doc(db, 'posts', postId as string), (snapshot) => {
      setPost(snapshot.data() || null);
    });
    setIsLoading(false);

    return unsubscribe;
  }, [db, postId]);

  return (
    <>
      <Head>
        <title>Twitter</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="min-h-screen flex max-w-[1500px] mx-auto">
        {
        session ? (
          <>
            <Sidebar />
            <section className="max-w-2xl grow-up px-3 py-3 border-l border-r border-gray-200 ml-[73px] xl:ml-[370px] flex-grow ">
              <Header />
              {
                !isLoading ? (
                  post && (
                  <>
                    <Post postId={postId as string} inDetail content={post as PostType} />
                    <Comments postId={postId as string} />
                  </>
                  )
                ) : (
                  <div className="flex justify-center items-center pt-24 m-auto">
                    <Image src={Loading} alt="loading" width={50} height={50} />
                  </div>
                )
              }
            </section>
            {suggestedFollowers && <Widgets suggestedFollowers={suggestedFollowers} />}
          </>
        ) : <Login providers={providers} />
       }
        <Modals />
      </main>
    </>
  );
};

export default PostPage;

export const getServerSideProps = async () => {
  const providers = await getProviders();
  const suggestedFollowers = await axios.get('https://jsonkeeper.com/b/TDY0');

  return {
    props: {
      providers,
      suggestedFollowers: suggestedFollowers.data,
    },
  };
};
