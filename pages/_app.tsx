import type { AppProps } from 'next/app';
import { SessionProvider } from 'next-auth/react';
import { RecoilRoot } from 'recoil';

import '../styles/globals.css';

const App = ({
  Component, pageProps: { session, ...pageProps },
}: AppProps) => (
  <SessionProvider session={session}>
    <RecoilRoot>
      <Component {...pageProps} />
    </RecoilRoot>
  </SessionProvider>
);

export default App;
