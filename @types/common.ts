import { Timestamp } from '@firebase/firestore';
import { ClientSafeProvider, LiteralUnion } from 'next-auth/react';
import { BuiltInProviderType } from 'next-auth/providers';

export interface Post {
      id: string;
      username: string;
      userImage: string;
      tag: string;
      text: string;
      timestamp: Timestamp | null ;
      image: string;
}

export type NextAuthProviders= Record<LiteralUnion<BuiltInProviderType, string>, ClientSafeProvider>

export interface Follower {
  userImage: string;
  username: string;
  tag: string;
}
