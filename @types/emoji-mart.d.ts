declare module '@emoji-mart/react' {
  interface EmojiMartProps{
    onEmojiSelect: any;
    data: any;
    theme: any;
    previewPosition: any;
  }

  declare class Picker extends React.Component<EmojiMartProps> {
    public constructor();
  }

  export default Picker;
}
